#!/bin/bash
kill $(ps aux | grep 'dotnet.*staging' | awk '{print $2}')
cd /home/materialorder/web/staging/contractors-api/current && nohup dotnet /home/materialorder/web/staging/contractors-api/current/MaterialOrder.Web.Contractors.dll >>  /home/materialorder/web/staging/contractors-api/shared/log/web.log 2>&1 </dev/null & 
cd /home/materialorder/web/staging/distributors-api/current && nohup dotnet /home/materialorder/web/staging/distributors-api/current/MaterialOrder.Web.Distributors.dll >>  /home/materialorder/web/staging/distributors-api/shared/log/web.log 2>&1 </dev/null &
sleep 5
pkill staging-post
