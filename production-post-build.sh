#!/bin/bash
kill $(ps aux | grep 'dotnet.*web/[c|d]' | awk '{print $2}')
cd /home/materialorder/web/contractors-api/current && nohup dotnet /home/materialorder/web/contractors-api/current/MaterialOrder.Web.Contractors.dll >>  /home/materialorder/web/contractors-api/shared/log/web.log 2>&1 </dev/null & 
cd /home/materialorder/web/distributors-api/current && nohup dotnet /home/materialorder/web/distributors-api/current/MaterialOrder.Web.Distributors.dll >>  /home/materialorder/web/distributors-api/shared/log/web.log 2>&1 </dev/null &
sleep 5
pkill production-post
